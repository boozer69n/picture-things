<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
		
		$this->loadModel('Images');
		$this->loadModel('Categories');
		$image = $this->Images->find('list', array('fields' => array('id', 'path')));
		$category = $this->Categories->find('list', array('fields' => array('id', 'category')));
		
		$this->set('image', $image);
		$this->set('category', $category);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$url = $this->file_is_valid('image');
			if ($url) {
				$this->request->data['User']['image'] = $url;
				$this->User->create();
				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash(__('The user has been saved.'));
//					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('Invalid image. Try again.'));
			}
		}
		
		$this->loadModel('Roles');		
		$roles = $this->Roles->find('list', array('fields' => array('id', 'type')));
		$this->set(compact('roles'));
	}
	
	private function file_is_valid($file) {
		if ($_FILES[$file]['error'] != UPLOAD_ERR_OK) {
			return false;
		}
		
		$sha = sha1_file($_FILES[$file]['tmp_name']);
		$file_url = WWW_ROOT . 'img' . DS . 'users' . DS . $sha;
		if (!move_uploaded_file($_FILES[$file]["tmp_name"], $file_url)) {
			return false;
		}		
		
		$file_url = str_replace(WWW_ROOT, '/', $file_url);
		$file_url = str_replace('\\', '/', $file_url);
		return $file_url;
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$url = $this->file_is_valid('image');
			if ($url) {
				$this->request->data['User']['image'] = $url;
				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash(__('The user has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('Invalid image. Try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$roles = $this->User->Role->find('list', 'type');
		$this->set(compact('roles'));
	}
	
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
