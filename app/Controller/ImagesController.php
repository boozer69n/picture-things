<?php
App::uses('AppController', 'Controller');
/**
 * Images Controller
 *
 * @property Image $Image
 * @property PaginatorComponent $Paginator
 */
class ImagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Image->recursive = 0;
		$this->set('images', $this->Image->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Image->exists($id)) {
			throw new NotFoundException(__('Invalid image'));
		}
		
		$options = array('conditions' => array('Image.id' => $id));
		$image = $this->Image->find('first', $options);
		
		$this->Image->clear(); //Image=model clears model
		$this->Image->id = $id;
		$this->Image->saveField('views', $image['Image']['views'] + 1);

		$image['Image']['views']++;
		
		$this->loadModel('User');
		$users = $this->User->find('list', array('fields' => array('id','image')));
		$nick = $this->User->find('list', array('fields' => array('id','nickname')));
		$this->set('image', $image);
		$this->set('users', $users);
		$this->set('nick', $nick);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$url = $this->file_is_valid('image');
			if ($url) {
				$this->request->data['Image']['path'] = $url;
				$this->Image->create();
				if ($this->Image->save($this->request->data)) {
					$this->Session->setFlash(__('The image has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The image could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('Invalid image. Try again.'));
			}
		}
		$users = $this->Image->User->find('list', array('fields' => array('id', 'nickname')));
		$categories = $this->Image->Category->find('list', array('fields' => array('id', 'category')));
		$this->set(compact('users', 'categories'));
	}
	
	private function file_is_valid($file) {
		if ($_FILES[$file]['error'] != UPLOAD_ERR_OK) {
			return false;
		}
		
		$sha = sha1_file($_FILES[$file]['tmp_name']);
		$file_url = WWW_ROOT . 'img' . DS . 'uploads' . DS . $sha;
		if (!move_uploaded_file($_FILES[$file]["tmp_name"], $file_url)) {
			return false;
		}		
		
		$file_url = str_replace(WWW_ROOT, '/', $file_url);
		$file_url = str_replace('\\', '/', $file_url);
		return $file_url;
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Image->exists($id)) {
			throw new NotFoundException(__('Invalid image'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Image->save($this->request->data)) {
				$this->Session->setFlash(__('The image has been saved.'));
				return $this->redirect(array('action' => 'view', $id));
			} else {
				$this->Session->setFlash(__('The image could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Image.' . $this->Image->primaryKey => $id));
			$this->request->data = $this->Image->find('first', $options);
		}
		$categories = $this->Image->Category->find('list', array('fields' => array('id', 'category')));
		$this->set(compact('users', 'categories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Image->id = $id;
		if (!$this->Image->exists()) {
			throw new NotFoundException(__('Invalid image'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Image->delete()) {
			$this->Session->setFlash(__('The image has been deleted.'));
		} else {
			$this->Session->setFlash(__('The image could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
