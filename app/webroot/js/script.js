$(document).ready(function() {
	$( "img" ).tooltip();	
	$( ".options a" ).button();	
	$( ".actions a" ).button();
	$("#sort").selectmenu({
		change: sortMenuChangeEvent,
		width: 300,
	});	

	$( "input[type='submit']" ).button();
	$( "input[type='file']" ).button();	
});

function sortMenuChangeEvent(event, ui) {
	window.location.replace(ui.item.value);
}