<!---------------------------------------------------------------------------->
<?php
$this->start('css');
	echo $this->Html->css('image_edit');
$this->end();

$this->start('script');
	echo $this->Html->script('script');
$this->end();
?>
<!---------------------------------------------------------------------------->
<div class="actions">
	<ul>
		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Image.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Image.id'))); ?></li>
		<li><?php echo $this->Html->link(__('Images'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
<!---------------------------------------------------------------------------->
<div class="images form center">
<?php echo $this->Form->create('Image'); ?>
	<fieldset>
		<legend class='title'><?php echo __('Edit Image'); ?></legend>
	<div class='center'><?php
		echo $this->Html->image($this->request->data['Image']['path'], array(
			'title' => $this->request->data['Image']['name'],
		)); ?></div>
		
		<div><?php echo $this->Form->input('id'); ?></div>
		<div><?php echo $this->Form->input('name'); ?></div>
		<div><?php echo $this->Form->input('description'); ?></div>
		<div ><?php echo $this->Form->input('category_id'); ?></div>
	</fieldset>
<div class='sub'><?php echo $this->Form->end(__('Submit')); ?></div>
</div>
<!---------------------------------------------------------------------------->