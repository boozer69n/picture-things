<!---------------------------------------------------------------------------->
<?php
$this->start('css');
	echo $this->Html->css('image_add');
$this->end();

$this->start('script');
	echo $this->Html->script('script');
$this->end();
?>
<!---------------------------------------------------------------------------->
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Images'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
<!---------------------------------------------------------------------------->
<div class="form center">
	<?php echo $this->Form->create('Image', array('type' => 'file')); ?>
	<fieldset>
		<legend class='title'><?php echo __('Add Image'); ?></legend>
		<label for="image">Picture (2MB max)</label>
		<div><input type="file" name="image" /></div>
		
				
		<div><?php echo $this->Form->input('name'); ?></div>
		<div><?php echo $this->Form->input('description'); ?></div>
		<div><?php echo $this->Form->input('user_id'); ?></div>
		<div class='cat'><?php echo $this->Form->input('category_id'); ?></div>

	</fieldset>
	<div class='submit center'><?php echo $this->Form->end(__('Submit')); ?></div>
</div>
<!---------------------------------------------------------------------------->