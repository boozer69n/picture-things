<?php
$this->start('css');
	echo $this->Html->css('image_index');
$this->end();

$this->start('script');
	echo $this->Html->script('script');
$this->end();
?>
<!---------------------------------------------------------------------------->
<div class="actions">
	<ul class='options'>
		<li><?php echo $this->Html->link(__('New Image'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
	</ul>
	<select id='sort'>
		<option>Sort Pictures...</option>
		<option value='<?php echo $this->Paginator->url(array('sort' => 'name')); ?>'>Name</option>
		<option value='<?php echo $this->Paginator->url(array('sort' => 'points')); ?>'>Points</option>
		<option value='<?php echo $this->Paginator->url(array('sort' => 'views')); ?>'>Views</option>
		<option value='<?php echo $this->Paginator->url(array('sort' => 'user_id')); ?>'>User</option>
		<option value='<?php echo $this->Paginator->url(array('sort' => 'category_id')); ?>'>Category</option>
	</select>
</div>
<!---------------------------------------------------------------------------->	
<div class='panel center'>
	<div cellpadding="0" cellspacing="0" class='area'>	
		<?php foreach ($images as $image): ?>
			<div class='image center'><?php
				echo $this->Html->image($image['Image']['path'], array(
					'alt' => $image['Image']['name'],
					'title' => $image['Image']['name'],
					'url' => array('action' => 'view', $image['Image']['id']),
				));
			?></div>
		<?php endforeach; ?>
	</div>
	<br style="clear: both;" />
</div>
<!---------------------------------------------------------------------------->