<?php
$this->start('css');
	echo $this->Html->css('image_view');
$this->end();

$this->start('script');
	echo $this->Html->script('script');
$this->end();
?>
<!---------------------------------------------------------------------------->
<div class="actions">
	<ul class='options'>
		<li><?php echo $this->Html->link(__('Edit Image'), array('action' => 'edit', $image['Image']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Image'), array('action' => 'delete', $image['Image']['id']), array(), __('Are you sure you want to delete # %s?', $image['Image']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Images'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Image'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
	</ul>
</div>
<!---------------------------------------------------------------------------->
<div class="panel center">
	<h2 class='name'><?php echo h($image['Image']['name']); ?></h2>
	
	<div class='image'>
		<?php
			echo $this->Html->image($image['Image']['path']);
		?>
	</div>
<!---------------------------------------------------------------------------->
	<div class='info'>
		<ul class='stats'>
			<li><?php echo __('Points: '); ?>
				<?php echo h($image['Image']['points']); ?>
				&nbsp;
			</li>
			<li><?php echo __('Views: '); ?>
				<?php echo h($image['Image']['views']); ?>
				&nbsp;
			</li>
			<li><?php echo __('User: '); ?>
				<?php echo $this->Html->link($image['User']['nickname'], array('controller' => 'users', 'action' => 'view', $image['User']['id'])); ?>
				&nbsp;
			</li>
			<li><?php echo __('Category: '); ?>
				<?php echo $this->Html->link($image['Category']['category'], array('controller' => 'categories', 'action' => 'view', $image['Category']['id'])); ?>
				&nbsp;
			</li>			
			
			<p><?php echo __('Description:'); ?></p>
			<p class='description'>
				<?php echo h($image['Image']['description']); ?>
				&nbsp;
			</p>
		</ul>
	</div>
</div>
<!---------------------------------------------------------------------------->
<div class="comments center">
	<h2><?php echo __('Comments'); ?></h2>
	<?php if (!empty($image['Comment'])): ?>
	<?php foreach ($image['Comment'] as $comment): ?>
		<div class='com_box center'>
			<?php
				echo $this->Html->image($users[$comment['user_id']], array(
					'title' => $nick[$comment['user_id']],
					'url' => array('controller' => 'users', 'action' => 'view', //stuff
					$comment['user_id']),
					'class' => 'com_pic',
				));
			?>
			<div><?php echo $comment['text']; ?></div>
			<div><?php echo $comment['created']; ?></div>
			<div><?php echo $comment['modified']; ?></div>
			<div class="actions">
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'comments', 'action' => 'edit', $comment['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'comments', 'action' => 'delete', $comment['id']), array(), __('Are you sure you want to delete # %s?', $comment['id'])); ?>
			</div>
		</div>
	<?php endforeach; ?>
	<?php endif; ?>
<!---------------------------------------------------------------------------->
	<div class="actions">
		<ul class='options'>
			<li><?php echo $this->Html->link(__('New Comment'), array('controller' => 'comments', 'action' => 'add', $image['Image']['id'])); ?> </li>
		</ul>
	</div>
</div>