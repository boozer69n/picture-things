<!---------------------------------------------------------------------------->
<?php
$this->start('css');
	echo $this->Html->css('com_add');
$this->end();

$this->start('script');
	echo $this->Html->script('script');
$this->end();
?>
<!---------------------------------------------------------------------------->
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul class='options'>
		<li><?php echo $this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('Images'), array('controller' => 'images', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Image'), array('controller' => 'images', 'action' => 'add')); ?> </li>
	</ul>
</div>
<!---------------------------------------------------------------------------->
<div class="form center">
<?php echo $this->Form->create('Comment'); ?>
	<fieldset>
		<legend class='title'><?php echo __('Add Comment'); ?></legend>
	<?php
		echo $this->Form->input('text');
		echo $this->Form->input('user_id');
		echo $this->Form->input('image_id', array('selected' => $imageId));
	?>
	</fieldset>
	<div class='sub'><?php echo $this->Form->end(__('Submit')); ?></div>
</div>
<!---------------------------------------------------------------------------->