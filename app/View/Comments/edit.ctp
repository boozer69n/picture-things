<!---------------------------------------------------------------------------->
<?php
$this->start('css');
	echo $this->Html->css('com_edit');
$this->end();

$this->start('script');
	echo $this->Html->script('script');
$this->end();
?>
<!---------------------------------------------------------------------------->
<div class="actions">
	<ul class='options'>
		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Comment.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Comment.id'))); ?></li>
		<li><?php echo $this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('Images'), array('controller' => 'images', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Image'), array('controller' => 'images', 'action' => 'add')); ?> </li>
	</ul>
</div>
<!---------------------------------------------------------------------------->
<div class="form center">
<?php echo $this->Form->create('Comment'); ?>
	<fieldset>
		<legend class='title'><?php echo __('Edit Comment'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('text');
		echo $this->Form->input('user_id');
		echo $this->Form->input('image_id');
	?>
	</fieldset>
	<div class='sub'><?php echo $this->Form->end(__('Submit')); ?></div>
</div>
<!---------------------------------------------------------------------------->