<!------------------------------------------------------------------------>
<?php
$this->start('css');
	echo $this->Html->css('cat_edit');
$this->end();

$this->start('script');
	echo $this->Html->script('script');
$this->end();
?>
<!------------------------------------------------------------------------>
<div class="actions">
	<ul>
		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Category.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Category.id'))); ?></li>
		<li><?php echo $this->Html->link(__('Categories'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Images'), array('controller' => 'images', 'action' => 'index')); ?> </li>
	</ul>
</div>
<!------------------------------------------------------------------------>
<div class="form center">
<?php echo $this->Form->create('Category'); ?>
	<fieldset>
		<legend class='title'><?php echo __('Edit Category'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('category');
		echo $this->Form->input('description');
	?>
	</fieldset>
	<div class='sub'><?php echo $this->Form->end(__('Submit')); ?></div>
</div>
<!------------------------------------------------------------------------>