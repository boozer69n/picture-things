<!------------------------------------------------------------------------>
<?php
$this->start('css');
	echo $this->Html->css('cat_view');
$this->end();

$this->start('script');
	echo $this->Html->script('script');
$this->end();
?>
<!------------------------------------------------------------------------>
<div class="title center">
	<h2><?php echo __('Category: '); echo ($category['Category']['category']);?></h2>
</div>
<!------------------------------------------------------------------------>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Edit Category'), array('action' => 'edit', $category['Category']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Category'), array('action' => 'delete', $category['Category']['id']), array(), __('Are you sure you want to delete # %s?', $category['Category']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Categories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('Images'), array('controller' => 'images', 'action' => 'index')); ?> </li>
	</ul>
</div>
<!------------------------------------------------------------------------>
<div class="panel center">
	<?php if (!empty($category['Image'])): ?>	
	<?php foreach ($category['Image'] as $image): ?>
		<div class='image'><?php
			echo $this->Html->image($image['path'], array(
			'title' => $image['name'],
			'url' => array('controller' => 'images', 'action' => 'view', $image['id']),
			));
		?></div>
<!------------------------------------------------------------------------>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
<!------------------------------------------------------------------------>