<!------------------------------------------------------------------------>
<?php
$this->start('css');
	echo $this->Html->css('cat_add');
$this->end();

$this->start('script');
	echo $this->Html->script('script');
$this->end();
?>
<!------------------------------------------------------------------------>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Categories'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Images'), array('controller' => 'images', 'action' => 'index')); ?> </li>
	</ul>
</div>
<!------------------------------------------------------------------------>
<div class="form center">
<?php echo $this->Form->create('Category'); ?>
	<fieldset>
		<legend class='title'><?php echo __('Add Category'); ?></legend>
	<?php
		echo $this->Form->input('category');
		echo $this->Form->input('description');
	?>
	</fieldset>
	<div class='sub center'><?php echo $this->Form->end(__('Submit')); ?></div>
</div>
<!------------------------------------------------------------------------>