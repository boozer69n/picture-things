<?php
$this->start('css');
	echo $this->Html->css('cat_index');
$this->end();

$this->start('script');
	echo $this->Html->script('script');
$this->end();
?>
<!------------------------------------------------------------------------>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('New Category'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('Images'), array('controller' => 'images', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Image'), array('controller' => 'images', 'action' => 'add')); ?> </li>
	</ul>
</div>
<!------------------------------------------------------------------------>
<div class="list">
	<h2 class='title'><?php echo __('Categories'); ?></h2>
	<table cellpadding="1" cellspacing="1">
	<?php foreach ($categories as $category): ?>
		<tr>
			<td><?php echo h($category['Category']['category']); ?>&nbsp;</td>
			<td><?php echo h($category['Category']['description']); ?>&nbsp;</td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('action' => 'view', $category['Category']['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $category['Category']['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $category['Category']['id']), array(), __('Are you sure you want to delete # %s?', $category['Category']['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
</div>
<!------------------------------------------------------------------------>