<!---------------------------------------------------------------------------->
<?php
$this->start('css');
	echo $this->Html->css('user_view');
$this->end();

$this->start('script');
	echo $this->Html->script('script');
$this->end();
?>
<!---------------------------------------------------------------------------->
<div class="actions">
	<ul class='options'>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('Images'), array('controller' => 'images', 'action' => 'index')); ?> </li>
	</ul>
</div>
<!---------------------------------------------------------------------------->
<div class="panel center">
	<h2 class='title'><?php echo __('User'); ?></h2>
	<div class='pic'><?php
		echo $this->Html->image($user['User']['image'], array(
			'title' => h($user['User']['nickname']),
		)); 
	?></div>

	<div class='info'>
		<div><strong><?php echo __('Username: ');?></strong><?php echo h($user['User']['username']);?></div>
		<div><strong><?php echo __('Last Online: ');?></strong><?php echo h($user['User']['last_online']);?></div>
		<div><strong><?php echo __('Nickname: ');?></strong><?php echo h($user['User']['nickname']);?></div>
		<div><strong><?php echo __('Email: ');?></strong><?php echo h($user['User']['email']);?></div>
		<div><strong><?php echo __('Created: ');?></strong><?php echo h($user['User']['created']);?></div>
		<div><strong><?php echo __('Points: ');?></strong><?php echo h($user['User']['points']);?></div>
		<div><strong><?php echo __('Role: ');?></strong><?php echo $user['Role']['type'];?></div>
	</div>
</div>
<div style="clear: both;"></div>
<!---------------------------------------------------------------------------->
<div class="panel center">
	<h3 class='title'><?php echo __('Images Commented On'); ?></h3>
	<?php if (!empty($user['Comment'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<?php foreach ($user['Comment'] as $comment): ?>
			<div class='image'><?php				 
				echo $this->Html->image($image[$comment['image_id']], array(
					'title' => $comment['text'],
					'url' => array('controller' => 'comments', 'action' => 'view', $comment['id']),
				));
			?></div>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
</div>
<!---------------------------------------------------------------------------->
<div class="panel center">
	<h3 class='title'><?php echo __('Posted Images'); ?></h3>
	<?php if (!empty($user['Image'])): ?>
	<div class='area'>
	<?php foreach ($user['Image'] as $image): ?>
		<div class='image'><?php			
			echo $this->Html->image($image['path'], array(
				'title' => $image['name'],
				'url' => array('controller' => 'images', 'action' => 'view', $image['id']),
			));
		?></div>
	<?php endforeach; ?>
	</div>
<?php endif; ?>
</div>
<!---------------------------------------------------------------------------->