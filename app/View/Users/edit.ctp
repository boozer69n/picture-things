<!---------------------------------------------------------------------------->
<?php
$this->start('css');
	echo $this->Html->css('user_edit');
$this->end();

$this->start('script');
	echo $this->Html->script('script');
$this->end();
?>
<!---------------------------------------------------------------------------->
<div class="actions">
	<ul class='options'>
		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
		<li><?php echo $this->Html->link(__('Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Images'), array('controller' => 'images', 'action' => 'index')); ?> </li>
	</ul>
</div>
<!---------------------------------------------------------------------------->
<div class="form center">
<?php echo $this->Form->create('User', array('type' => 'file')); ?>
	<fieldset>
		<legend class='title'><?php echo __('Edit User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('nickname');
		echo $this->Form->input('email');	
		echo $this->Form->create('Image', array('type' => 'file'));?>
			<fieldset>
			<legend><?php echo __('Add Image'); ?></legend>
			<label for="image">Picture (2MB max)</label>
			<input type="file" name="image" />
	</fieldset>
	<div class='sub'><?php echo $this->Form->end(__('Submit')); ?></div>
</div>
<!---------------------------------------------------------------------------->