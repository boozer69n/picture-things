<!---------------------------------------------------------------------------->
<?php
$this->start('css');
	echo $this->Html->css('user_add');
$this->end();

$this->start('script');
	echo $this->Html->script('script');
$this->end();
?>
<!---------------------------------------------------------------------------->
<div class="actions">
	<ul class='options'>
		<li><?php echo $this->Html->link(__('Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Images'), array('controller' => 'images', 'action' => 'index')); ?> </li>
	</ul>
</div>
<!---------------------------------------------------------------------------->
<div class="form center">
<?php echo $this->Form->create('User', array('type' => 'file')); ?>
	<fieldset>
		<legend class='title'><?php echo __('Add User'); ?></legend>
	<?php
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('nickname');
		echo $this->Form->input('email');
		echo $this->Form->input('image', array('type' => 'file', 'name' => 'image'));?>

	<?php
		echo $this->Form->input('role_id');
	?>
	</fieldset>
	<div class='sub'><?php echo $this->Form->end(__('Submit')); ?></div>
</div>
<!---------------------------------------------------------------------------->