<?php
/**
 * UserFixture
 *
 */
class UserFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'the id number of the userex: 1234567890'),
		'username' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'key' => 'unique', 'collate' => 'utf8_general_ci', 'comment' => 'the user name for the userex: cbooze', 'charset' => 'utf8'),
		'password' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'users passwordex: password1', 'charset' => 'utf8'),
		'last_online' => array('type' => 'timestamp', 'null' => false, 'comment' => 'date and time the user was last online

ex: 2014-10-23 20:11'),
		'nickname' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'key' => 'unique', 'collate' => 'utf8_general_ci', 'comment' => 'stores the nickname of the userex: boozer', 'charset' => 'utf8'),
		'email' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'key' => 'unique', 'collate' => 'utf8_general_ci', 'comment' => 'the users email

ex: fakeemail@gmail.com', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null, 'comment' => 'the date the account was createdex: 2014-10-23 20:17'),
		'points' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false, 'comment' => 'stores the points the user has for the amount of points their pictures have recieved'),
		'image' => array('type' => 'string', 'null' => true, 'default' => 'localhost\\picture%20things\\person_pics\\img0.jpeg', 'length' => 200, 'collate' => 'utf8_general_ci', 'comment' => 'stores the path to the image that is being used as the users id pictureex: C:\\folder1\\folder2\\pic1.jpg', 'charset' => 'utf8'),
		'role_id' => array('type' => 'biginteger', 'null' => true, 'default' => '0', 'unsigned' => true, 'key' => 'index', 'comment' => 'pulling the access level from the access table'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'idaccount_UNIQUE' => array('column' => 'id', 'unique' => 1),
			'account_name_UNIQUE' => array('column' => 'username', 'unique' => 1),
			'nickname_UNIQUE' => array('column' => 'nickname', 'unique' => 1),
			'email_UNIQUE' => array('column' => 'email', 'unique' => 1),
			'access_level_idx' => array('column' => 'role_id', 'unique' => 0),
			'role_id' => array('column' => 'role_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'username' => 'Lorem ipsum dolor sit amet',
			'password' => 'Lorem ipsum dolor sit amet',
			'last_online' => 1415487555,
			'nickname' => 'Lorem ipsum dolor sit amet',
			'email' => 'Lorem ipsum dolor sit amet',
			'created' => '2014-11-08 23:59:15',
			'points' => 1,
			'image' => 'Lorem ipsum dolor sit amet',
			'role_id' => ''
		),
	);

}
