<?php
/**
 * ImageFixture
 *
 */
class ImageFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary', 'comment' => 'incriments numbers to be assigned to the images so no file has the same nameex: img1, img2... img3567'),
		'path' => array('type' => 'string', 'null' => false, 'default' => 'localhost\\\\picture things\\\\uploaded_images\\\\img0.jpg', 'length' => 200, 'key' => 'unique', 'collate' => 'utf8_general_ci', 'comment' => 'stores the path and file name of the uploaded image', 'charset' => 'utf8'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'comment' => 'the name of the image

ex: "The Grand Canyon at Night"', 'charset' => 'utf8'),
		'points' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false, 'comment' => 'the number of people that "liked" the picture, basically when someone clicks the thumbs up button'),
		'views' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false, 'comment' => 'the number of people that have viewed the image

ex: 523 views'),
		'description' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => 'the description the user supplies as a description of the pictureex: "I took this picture while I was camping near the rim of the Grand Canyon with my family"', 'charset' => 'utf8'),
		'user_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index', 'comment' => 'the id/account number of the user that uploaded the picture'),
		'category_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'idimages_UNIQUE' => array('column' => 'id', 'unique' => 1),
			'image_UNIQUE' => array('column' => 'path', 'unique' => 1),
			'user_id_idx' => array('column' => 'user_id', 'unique' => 0),
			'category_id' => array('column' => 'category_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'path' => 'Lorem ipsum dolor sit amet',
			'name' => 'Lorem ipsum dolor sit amet',
			'points' => 1,
			'views' => 1,
			'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'user_id' => '',
			'category_id' => ''
		),
	);

}
